package fr.craftinglabs.apps.zonetransition.actions;

import org.junit.Test;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.zone.ZoneOffsetTransition;

import static org.junit.Assert.assertEquals;

public class GetNextTransitionTest {

    @Test
    public void
    should_return_next_transition() {
        GetNextTransition action = new GetNextTransition();
        Instant instant = Instant.parse("2019-01-01T00:00:00.00Z");
        ZoneId zone = ZoneId.of("Europe/Paris");

        ZoneOffsetTransition transition = action.next(zone, instant);

        ZoneOffsetTransition expected = ZoneOffsetTransition.of(LocalDateTime.parse("2019-03-31T02:00"), ZoneOffset.ofHours(1), ZoneOffset.ofHours(2));
        assertEquals(expected, transition);
    }
}
