package fr.craftinglabs.apps.zonetransition.actions;

import org.junit.Test;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.zone.ZoneOffsetTransition;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class GetTransitionsTest {

    @Test
    public void
    should_return_current_and_next_transition() {
        GetTransitions action = new GetTransitions();
        Instant instant = Instant.parse("2019-01-01T00:00:00.00Z");
        ZoneId zone = ZoneId.of("Europe/Paris");

        List<ZoneOffsetTransition> transition = action.transitions(zone, instant);

        assertEquals(expectedTransitions(), transition);
    }

    @Test
    public void
    should_return_current_and_next_transition_for_Istanbul() {
        GetTransitions action = new GetTransitions();
        Instant instant = Instant.parse("2019-01-01T00:00:00.00Z");
        ZoneId zone = ZoneId.of("Europe/Istanbul");

        List<ZoneOffsetTransition> transitions = action.transitions(zone, instant);

        List<ZoneOffsetTransition> expected = asList(ZoneOffsetTransition.of(LocalDateTime.parse("2016-03-27T03:00"), ZoneOffset.ofHours(2), ZoneOffset.ofHours(3)));
        assertEquals(expected, transitions);
    }

    List<ZoneOffsetTransition> expectedTransitions() {
        List<ZoneOffsetTransition> transitions = new ArrayList<>();
        transitions.add(ZoneOffsetTransition.of(LocalDateTime.parse("2018-10-28T03:00"), ZoneOffset.ofHours(2), ZoneOffset.ofHours(1)));
        transitions.add(ZoneOffsetTransition.of(LocalDateTime.parse("2019-03-31T02:00"), ZoneOffset.ofHours(1), ZoneOffset.ofHours(2)));
        transitions.add(ZoneOffsetTransition.of(LocalDateTime.parse("2019-10-27T03:00"), ZoneOffset.ofHours(2), ZoneOffset.ofHours(1)));
        transitions.add(ZoneOffsetTransition.of(LocalDateTime.parse("2020-03-29T02:00"), ZoneOffset.ofHours(1), ZoneOffset.ofHours(2)));
        transitions.add(ZoneOffsetTransition.of(LocalDateTime.parse("2020-10-25T03:00"), ZoneOffset.ofHours(2), ZoneOffset.ofHours(1)));
        transitions.add(ZoneOffsetTransition.of(LocalDateTime.parse("2021-03-28T02:00"), ZoneOffset.ofHours(1), ZoneOffset.ofHours(2)));

        return transitions;
    }
}
