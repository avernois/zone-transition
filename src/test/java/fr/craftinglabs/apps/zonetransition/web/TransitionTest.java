package fr.craftinglabs.apps.zonetransition.web;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.zone.ZoneOffsetTransition;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class TransitionTest {

    @Test public void
    should_have_have_the_correct_offset() {
        ZoneOffsetTransition original = ZoneOffsetTransition.of(LocalDateTime.parse("2019-03-31T02:00"), ZoneOffset.ofHours(1), ZoneOffset.ofHours(2));

        Transition transition = Transition.from(original);

        assertEquals(7200, transition.offset());
    }

    @Test public void
    should_have_have_the_correct_epoch() {
        ZoneOffsetTransition original = ZoneOffsetTransition.of(LocalDateTime.parse("2019-03-31T02:00"), ZoneOffset.ofHours(1), ZoneOffset.ofHours(2));

        Transition transition = Transition.from(original);

        assertEquals(LocalDateTime.parse("2019-03-31T02:00").toEpochSecond(ZoneOffset.ofHours(1)), transition.epoch());
    }

    @Test public void
    should_return_a_list_from_a_list() {
        List<ZoneOffsetTransition> original = new ArrayList<>();
        original.add(ZoneOffsetTransition.of(LocalDateTime.parse("2019-03-31T02:00"), ZoneOffset.ofHours(1), ZoneOffset.ofHours(2)));
        original.add(ZoneOffsetTransition.of(LocalDateTime.parse("2019-10-27T03:00"), ZoneOffset.ofHours(2), ZoneOffset.ofHours(1)));

        List<Transition> transitions = Transition.from(original);

        assertEquals(original.size(), transitions.size());
        for(int i =0; i < original.size(); i++) {
            assertEquals(transitions.get(i), Transition.from(original.get(i)));
        }
    }
}
