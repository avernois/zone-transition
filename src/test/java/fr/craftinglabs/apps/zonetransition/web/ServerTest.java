package fr.craftinglabs.apps.zonetransition.web;

import com.google.gson.Gson;
import fr.craftinglabs.apps.zonetransition.actions.GetNextTransition;
import fr.craftinglabs.apps.zonetransition.actions.GetTransitions;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.zone.ZoneOffsetTransition;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ServerTest {

    private static final ZoneOffsetTransition ZONE_OFFSET_TRANSITION = ZoneOffsetTransition.of(LocalDateTime.parse("2019-03-31T02:00"), ZoneOffset.ofHours(1), ZoneOffset.ofHours(2));
    public static final int SERVER_PORT = 4567;
    public static final String SERVER_URL = "http://localhost:" + SERVER_PORT;
    private Server server;
    private final Gson gson = new Gson();
    private GetTransitions getTransitions;
    private GetNextTransition nextTransition;

    @Before
    public void setUp() {
        getTransitions = mock(GetTransitions.class);
        nextTransition = mock(GetNextTransition.class);
        server = new Server(nextTransition, getTransitions);
        server.start(SERVER_PORT);
    }

    @Test public void
    should_answer_to_ping() throws IOException {
        HttpResponse response = getResponseFromURI(SERVER_URL + "/ping");

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test public void
    should_return_next_transition_as_json_for_a_given_zone() throws IOException {
        when(nextTransition.next(any(), any())).thenReturn(ZONE_OFFSET_TRANSITION);
        String expected = gson.toJson(Transition.from(ZONE_OFFSET_TRANSITION));

        HttpResponse response = getResponseFromURI(SERVER_URL + "/Europe%2FParis/next");
        String content = EntityUtils.toString(response.getEntity());

        verify(nextTransition).next(eq(ZoneId.of("Europe/Paris")), any());
        assertEquals(200, response.getStatusLine().getStatusCode());
        assertEquals(expected.length(), getContentLength(response));
        assertEquals(expected, content);
    }
    
    @Test public void
    should_return_previous_and_next_transition_as_json_for_a_given_zone() throws IOException {
        when(getTransitions.transitions(any(), any())).thenReturn(zoneTransitions());
        String expected = gson.toJson(Transition.from(zoneTransitions()));

        HttpResponse response = getResponseFromURI(SERVER_URL + "/Europe%2FParis");
        String content = EntityUtils.toString(response.getEntity());

        verify(getTransitions).transitions(ArgumentMatchers.eq(ZoneId.of("Europe/Paris")), any());
        assertEquals(200, response.getStatusLine().getStatusCode());
        assertEquals(expected.length(), getContentLength(response));
        assertEquals(expected, content);
    }

    private long getContentLength(HttpResponse response) {
        return Integer.valueOf(response.getFirstHeader("Content-Length").getValue());
    }

    @Test public void
    should_return_erreur_404_when_transtion_zone_does_not_exist() throws IOException {
        HttpResponse response = getResponseFromURI(SERVER_URL + "/Thiberville");

        assertEquals(404, response.getStatusLine().getStatusCode());
    }

    private HttpResponse getResponseFromURI(String URI) throws IOException {
        HttpGet request = new HttpGet(URI);
        HttpClient client = HttpClientBuilder.create().build();

        return client.execute(request);
    }

    @After
    public void tearDown() {
        if (server != null)
            server.stop();
    }

    private List<ZoneOffsetTransition> zoneTransitions() {
        List<ZoneOffsetTransition> transitions = new ArrayList<>();
        transitions.add(ZoneOffsetTransition.of(LocalDateTime.parse("2019-03-31T02:00"), ZoneOffset.ofHours(1), ZoneOffset.ofHours(2)));
        transitions.add(ZoneOffsetTransition.of(LocalDateTime.parse("2019-10-27T03:00"), ZoneOffset.ofHours(2), ZoneOffset.ofHours(1)));
        transitions.add(ZoneOffsetTransition.of(LocalDateTime.parse("2020-03-29T02:00"), ZoneOffset.ofHours(1), ZoneOffset.ofHours(2)));
        transitions.add(ZoneOffsetTransition.of(LocalDateTime.parse("2020-10-25T03:00"), ZoneOffset.ofHours(2), ZoneOffset.ofHours(1)));
        transitions.add(ZoneOffsetTransition.of(LocalDateTime.parse("2021-03-28T02:00"), ZoneOffset.ofHours(1), ZoneOffset.ofHours(2)));
        transitions.add(ZoneOffsetTransition.of(LocalDateTime.parse("2021-10-31T03:00"), ZoneOffset.ofHours(2), ZoneOffset.ofHours(1)));

        return transitions;
    }
}
