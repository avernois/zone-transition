package fr.craftinglabs.apps.zonetransition.actions;

import java.time.Instant;
import java.time.ZoneId;
import java.time.zone.ZoneOffsetTransition;
import java.util.ArrayList;
import java.util.List;

public class GetTransitions {
    public List<ZoneOffsetTransition> transitions(ZoneId zone, Instant instant) {
        List<ZoneOffsetTransition> transitions = new ArrayList<>();

        ZoneOffsetTransition transition = zone.getRules().previousTransition(instant);
        int count = 0;
        while(transition != null && count < 6) {
            transitions.add(transition);
            transition = zone.getRules().nextTransition(transition.getInstant());
            count++;
        }

        return transitions;
    }
}
