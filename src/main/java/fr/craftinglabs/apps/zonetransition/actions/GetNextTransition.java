package fr.craftinglabs.apps.zonetransition.actions;

import java.time.Instant;
import java.time.ZoneId;
import java.time.zone.ZoneOffsetTransition;

public class GetNextTransition {
    public ZoneOffsetTransition next(ZoneId zone, Instant instant) {
        return zone.getRules().nextTransition(instant);
    }
}
