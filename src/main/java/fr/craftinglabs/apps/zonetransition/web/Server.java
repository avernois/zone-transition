package fr.craftinglabs.apps.zonetransition.web;

import com.google.gson.Gson;
import fr.craftinglabs.apps.zonetransition.actions.GetNextTransition;
import fr.craftinglabs.apps.zonetransition.actions.GetTransitions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Spark;

import java.time.Instant;
import java.time.ZoneId;
import java.time.zone.ZoneOffsetTransition;
import java.time.zone.ZoneRulesException;
import java.util.List;

import static spark.Spark.*;


public class Server {
    private static final Logger LOGGER = LoggerFactory.getLogger(Server.class);
    private static final int DEFAULT_PORT = 5000;
    private GetNextTransition nextTransition;
    private GetTransitions getTransitions;

    public Server(GetNextTransition nextTransition, GetTransitions getTransitions) {
        this.nextTransition = nextTransition;
        this.getTransitions = getTransitions;
    }

    public void start(int port) {
        Gson gson = new Gson();
        port(port);
        get("/ping", (request, response) -> "pong");

        get("/:zone/next", ((request, response) -> {
            String zoneName = request.params("zone");
            LOGGER.info("GET /{}/next", zoneName);
            Transition transition = Transition.from(nextTransition.next(ZoneId.of(zoneName), Instant.now()));

            return transition;
        }), gson::toJson);

        get("/:zone", ((request, response) -> {
            String zoneName = request.params("zone");
            LOGGER.info("GET /{}", zoneName);
            List<ZoneOffsetTransition> zoneTransitions = getTransitions.transitions(ZoneId.of(zoneName), Instant.now());

            return Transition.from(zoneTransitions);
        }), gson::toJson);

        exception(ZoneRulesException.class, (zoneRulesException, request, response) -> {
            response.status(404);
            LOGGER.warn(zoneRulesException.getMessage());
            response.body("Requested Zone does not exist.");
        });

        after((request, response) -> {
            if(response.body() != null)
                response.header("Content-Length", String.valueOf(response.body().length()));
        });

        awaitInitialization();
    }

    public void stop() {
        Spark.stop();
        Spark.awaitStop();
    }

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Initializing");
        GetNextTransition nextTransition = new GetNextTransition();
        GetTransitions transitions = new GetTransitions();

        Server server = new Server(nextTransition, transitions);
        int port = getPort();
        LOGGER.info("Starting server on port {}", port);
        server.start(port);
        addShutdownHook(server);
        Thread.currentThread().join();
    }

    private static int getPort() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get("PORT") != null) {
            return Integer.parseInt(processBuilder.environment().get("PORT"));
        }
        return DEFAULT_PORT;
    }

    private static void addShutdownHook(Server server) {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Exiting.");
            server.stop();
        }, "shutdownHook"));
    }
}
