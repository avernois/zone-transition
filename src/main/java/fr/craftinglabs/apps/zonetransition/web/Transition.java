package fr.craftinglabs.apps.zonetransition.web;

import java.time.zone.ZoneOffsetTransition;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Transition {
    private final long epoch;
    private final int offset;

    private Transition(long epoch, int offset) {
        this.epoch = epoch;
        this.offset = offset;
    }

    public static Transition from(ZoneOffsetTransition original) {
        return new Transition(original.getInstant().getEpochSecond(), original.getOffsetAfter().getTotalSeconds());
    }

    public static List<Transition> from(List<ZoneOffsetTransition> original) {
        List<Transition> transitions = new ArrayList<>();
        for(ZoneOffsetTransition zoneTransition : original) {
            transitions.add(Transition.from(zoneTransition));
        }
        return transitions;
    }

    public int offset() {
        return offset;
    }

    public long epoch() {
        return epoch;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transition that = (Transition) o;
        return epoch == that.epoch &&
                offset == that.offset;
    }

    @Override
    public int hashCode() {
        return Objects.hash(epoch, offset);
    }
}
