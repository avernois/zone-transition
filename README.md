# Zone transition

Zone transition is a small API over http to retrieve transitions for a time zone.

## Context

I make [clocks](https://gitlab.com/avernois/clocks), often with esp8266 and leds.

In order to follow change of time offset (like daylight saving time), I needed to have those information available.
As these information might change quite often, being able to retrieve them from an outside updated source seems a good idea (in opposition to storing once the tz database with the chipset).

## API documentation

### Zoneid

ZoneId is a string representing the id of a Time zone.
These ids are the ones defined by IANA.
They are case sensitive.

> note: as ZoneIds contain '/' they should be URL encoded when sent to the server.

> note: data for the ZoneId and transition come from the tz database embeded in the Java runtime.

### Transition

A `Transition` contains usefull information to define when a time offset transition will occur. 

Example:
```json
{
  "epoch":1553994000,
  "offset":7200
}
```

* `epoch` is the timestamp (UTC) of the time the transition will occur.
* `offset` it the time offset in seconds that will apply after once the transition has occured.

### Get current and next transition for a time zone
Get the previous and next 5 transitions for the zone.

```http request
GET /:zone
```

where `:zone` is the zone id of the timezone.

example:

```http request
GET /Europe%2FParis
```
will return a list of 6 transitions (the previous one and the 5 nexts)

### Get next transition for a time zone
Get the next transition for a time zone.

```http request
GET /:zone/next
```
where `:zone` is the zone id of the timezone.

### Ping
```http request
GET /ping
```

Returns the string "pong"

## Building and running

The server is written in java and use Maven to build.

```
mvn clean package
```
will produce an executable jar in the `target/` directory.

Once build:
```
java -jar target/zonetransition-1.0-SNAPSHOT.jar
```
will start the server.

By default, the server is listening on port 5000. You can specify another port by defining the `PORT` variable environment prior to starting the server.

> note: for the moment, I do not plan on taking care of updating the version number (1.0-SNAPSHOT), so you should not rely on it for any purpose. This might change in the future.

> note: it is ready to be deployed on [heroku](https://heroku.com): create an app, push the code of the main branch and it should work.

## Licence

'Zone transtion' is released under [The Hippocratic License](https://firstdonoharm.dev).